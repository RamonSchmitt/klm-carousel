import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgReduxModule, NgRedux } from '@angular-redux/store';
import { Store, createStore } from 'redux';
import { rootReducer } from './reducers';
import { AppComponent } from './app.component';
import { CarouselComponent } from './carousel/carousel.component';
import { Slide } from './models';
import { SlidesActions } from './actions';

export interface AppState {
	slides: Slide[];
}

export const store: Store<AppState> = createStore(rootReducer);
@NgModule({
	declarations: [
		AppComponent,
		CarouselComponent
	],
	imports: [
		BrowserModule,
		NgReduxModule,
	],
	providers: [
		SlidesActions
	],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor(ngRedux: NgRedux<AppState>) {
		ngRedux.provideStore(store);
	}
}
