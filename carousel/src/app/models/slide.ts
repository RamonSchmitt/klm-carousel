export interface Slide {
	image: string;
	alt: string;
	active: boolean;
}
