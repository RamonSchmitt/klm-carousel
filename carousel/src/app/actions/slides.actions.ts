import { Injectable } from '@angular/core';
import { dispatch } from '@angular-redux/store';
import { Slide } from '../models';

@Injectable()
export class SlidesActions {
	static readonly SET_SLIDES = 'SET_INGREDIENTS';

	@dispatch()
	setSlides(payload: Slide[]) {
		return {
			type: SlidesActions.SET_SLIDES,
			payload
		};
	}
}
