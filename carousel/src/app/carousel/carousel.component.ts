import { Component, OnInit, Input } from '@angular/core';
import { Slide } from '../models';

@Component({
	selector: 'klm-carousel',
	templateUrl: './carousel.component.html',
	styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
	@Input() slides = [];
	@Input() autoPlay: false;

	public slideIndex = 0;

	ngOnInit() {
		if (this.autoPlay) {
			setInterval(() => {
				this.browseSlides(1);
			}, 7000);
		}
	}

	browseSlides(number: number): void {
		this.showSlide(this.slideIndex += number);
	}

	selectSlide(number: number): void {
		this.slideIndex = number;
		this.showSlide(this.slideIndex);
	}

	showSlide(number: number): void {
		if (number > this.slides.length - 1) { this.slideIndex = 0; }
		if (number < 0) { this.slideIndex = this.slides.length - 1; }

		this.slides.forEach((slide: Slide) => slide.active = false);
		this.slides[this.slideIndex].active = true;
	}
}
