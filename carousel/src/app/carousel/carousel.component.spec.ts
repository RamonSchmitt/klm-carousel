import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselComponent } from './carousel.component';

describe('CarouselComponent', () => {
	let component: CarouselComponent;
	let fixture: ComponentFixture<CarouselComponent>;

	const slides = [
		{ image: 'assets/home1.jpg', alt: 'plane', active: true },
		{ image: 'assets/home2.jpg', alt: 'windmill', active: false },
		{ image: 'assets/home3.jpg', alt: 'london', active: false },
		{ image: 'assets/home4.jpg', alt: 'ipad', active: false },
		{ image: 'assets/home5.jpg', alt: 'couple', active: false },
	];

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CarouselComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CarouselComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should browse trough slides', () => {
		component.autoPlay = false;
		component.slides = slides;

		component.browseSlides(1);

		expect(component.slides[0].active).toBe(false);
		expect(component.slides[1].active).toBe(true);
	});

	it('should select a slide', () => {
		component.autoPlay = false;
		component.slides = slides;

		component.selectSlide(3);

		expect(component.slides[0].active).toBe(false);
		expect(component.slides[3].active).toBe(true);
	});
});
