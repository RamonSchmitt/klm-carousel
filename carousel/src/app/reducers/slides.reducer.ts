import { SlidesActions } from '../actions';

const INITIAL_STATE = [];

export function slides(state = INITIAL_STATE, action) {
	switch (action.type) {
		case SlidesActions.SET_SLIDES:
			console.log('payload = ', action.payload);
			return action.payload;
		default:
			return state;
	}
}
