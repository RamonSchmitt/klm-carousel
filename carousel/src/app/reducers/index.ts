import { combineReducers } from 'redux';
import { slides } from './slides.reducer';

export const rootReducer: any = combineReducers({
	slides,
});
