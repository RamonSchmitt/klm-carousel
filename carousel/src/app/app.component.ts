import { Component, OnInit } from '@angular/core';
import { Slide } from './models';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { SlideService } from './services/slide.service';

@Component({
	selector: 'klm-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	@select(state => state.slides)
	readonly slides$: Observable<Slide[]>;

	slides = [];

	constructor(
		private slidesService: SlideService
	) {}

	ngOnInit() {
		this.slidesService.getSlides();

		this.slides$.subscribe(slides => {
			this.slides = slides;
		});
	}
}
