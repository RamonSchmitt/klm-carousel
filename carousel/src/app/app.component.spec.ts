import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CarouselComponent } from './carousel/carousel.component';
import { SlidesActions } from './actions';

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent,
				CarouselComponent
			],
			providers: [
				SlidesActions,
			]
		}).compileComponents();
	}));

	it('should create the app', () => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	});
});
