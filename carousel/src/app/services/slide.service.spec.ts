import { TestBed } from '@angular/core/testing';

import { SlideService } from './slide.service';
import { SlidesActions } from '../actions';

describe('SlideService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		providers: [
			SlidesActions,
		]
	}));

	it('should be created', () => {
		const service: SlideService = TestBed.get(SlideService);
		expect(service).toBeTruthy();
	});
});
