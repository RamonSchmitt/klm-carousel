import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Slide } from '../models';
import { SlidesActions } from '../actions';

@Injectable({
	providedIn: 'root'
})
export class SlideService {
	private mockRequest = of([
		{ image: 'assets/home1.jpg', alt: 'plane', active: true },
		{ image: 'assets/home2.jpg', alt: 'windmill', active: false },
		{ image: 'assets/home3.jpg', alt: 'london', active: false },
		{ image: 'assets/home4.jpg', alt: 'ipad', active: false },
		{ image: 'assets/home5.jpg', alt: 'couple', active: false },
	]);

	constructor(
		private slidesActions: SlidesActions,
	) {}

	getSlides() {
		this.mockRequest.subscribe((slides: Slide[]) => {
			this.slidesActions.setSlides(slides);
		});
	}
}
